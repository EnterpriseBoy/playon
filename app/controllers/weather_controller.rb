class WeatherController < ApplicationController
	require 'open-uri'
	require 'json'

	def index
	end


	def create
		#Capture the response from the form
		cordinates= Geocoder.coordinates(params[:city_input])
		#Stop if an incorrect location is given
		if cordinates.nil?
			flash.now[:notice] = "Location does not exist"
			redirect_to :back
			return
		end
		
		#Store Latitude and Longitude
		lat= cordinates[0].to_s
		lng= cordinates[1].to_s
		key="72ee8bab77d416d99b868bba45f4c24e"
		#URI to Open
		uri = "http://api.openweathermap.org/data/2.5/forecast/daily?lat="+lat+"&lon="+lng+"&cnt=16&mode=json&appid="+key
		#Open the URI and Read contents 
		response = open(uri).read
		#Parse JSON Response
		totalWeather = JSON.parse(response)
		#Array of days
		@dayArray = []
		totalWeather['list'].each do |i|
			#Weather Hash for each day
			weatherDay = Hash.new('n/a')
				#weatherDay["humidity"] = i['humidity'].to_s
				weatherDay["day"] = Time.at(i['dt']).to_datetime.strftime("%a").to_s
				weatherDay["date"] = Time.at(i['dt']).to_datetime.strftime("%d").to_s
				weatherDay["month"] = Time.at(i['dt']).to_datetime.strftime("%b").to_s
				weatherDay["main"] = i['weather'][0]['main'].to_s
				weatherDay["tempDay"] = i['temp']['day'].to_s
				weatherDay["tempMin"] = i['temp']['min'].to_s
				weatherDay["desc"] = i['weather'][0]['description'].to_s
			@dayArray << weatherDay
		end
		session[:array] = @dayArray
		flash[:notice] = "location exists"
		render :index
	end

	def show()
		#Specify the specific day from the session in the array
		@weatherForDay = session[:array][params[:id].to_i]
		puts "this is a test" + @weatherForDay.to_s
		respond_to do |format|
			format.js
		end

	end
	
end
