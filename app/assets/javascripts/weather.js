$(document).on('ready', function() {

var xhttp = new XMLHttpRequest();

$('.owl-carousel').owlCarousel({
    rtl:true,
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
	

	// Create the search box and link it to the UI element.
	var input = document.getElementById('city_input');
	var autocomplete = new google.maps.places.Autocomplete(input);

	//Returns list of elements with class panel
	var panelList = document.querySelectorAll('.panel');
	//Default value is Null
	var paneStatusUpID=null;
	//Itterating through the array of ids
	for (var i = 0; i < panelList.length; i++) {
		panelList[i].addEventListener('click', function() {
			//No Panel is selected
			if (paneStatusUpID == null){
				//infoDiv(this.id);
				paneUp(this.id);
				paneStatusUpID=this.id;
			//Selecting the same panel
			}else if(this.id == paneStatusUpID){
				paneDown(paneStatusUpID);
				paneStatusUpID=null;
				infoDivHide(paneStatusUpID);
			}
			//Selecting a new panel
			else{
				infoDivHide(this.id);
				paneDownUp(paneStatusUpID,this.id)
				//paneDown(paneStatusUpID);
				//paneUp(this.id);
				//infoDiv(this.id);
				paneStatusUpID=this.id;
			}
			
		});
	}

	function paneUp(id){
		$("#"+id).animate({
				opacity: 0.25,
				bottom: "+=150",
			}, 1000, function() {
				infoDiv(id);
	  }	);
	}

	function paneDown(id){
		$("#"+id).animate({
				opacity: 1,
				bottom: "-=150",
			}, 1000, function() {
	    // Animation complete.
	  });
	}
	//Close one panel open new one
	function paneDownUp(idDown,idUp){
		$("#"+idDown).animate({
			opacity: 1,
			bottom: "-=150"
		}, 1000, function(){
			paneUp(idUp);
		});
	}

	function infoDiv(id){
		//Getting the current location on screen
		var x = $('#'+id).offset();
		$("#info").show(1000).css({
			'top' : x.top+150,
			'left' : x.left
		});
	}
	function infoDivHide(id){
		$("#info").hide(1000);
	}

});